﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.AR;

public class DestroyCheckpoints : MonoBehaviour
{

    private bool isSelected = false;
    private Text textDebug;

    [SerializeField] private Button buttonDelete;
    [SerializeField] private GameObject ARAnnotation;

    private GameObject currentCheckpoint;
    private ARPlacementInteractable ARPlacement;

    // Start is called before the first frame update
    void Start()
    {
        buttonDelete.onClick.AddListener(onClickDelete);
        ARPlacement = GameObject.Find("AR Placement Interactable").GetComponent<ARPlacementInteractable>();
        textDebug = GameObject.Find("Text objet interaction").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SelectedEnter(SelectEnterEventArgs args)
    {
        isSelected = true;
        ARAnnotation.SetActive(true);

        ARPlacement.enabled = false;

        textDebug.text = args.interactable.gameObject.name;
        currentCheckpoint = args.interactable.gameObject;


    }

    public void SelectedExit(SelectExitEventArgs args)
    {
        ARAnnotation.SetActive(false);
        isSelected = false;
        ARPlacement.enabled = true;
    } 

    private void onClickDelete()
    {
        textDebug.text += "ON A CLIQUE SUR DELETE";
        if(currentCheckpoint != null)
        {
            Destroy(currentCheckpoint);
        }
    }
}
