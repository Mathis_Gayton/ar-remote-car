﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArriveeInstantiated : MonoBehaviour
{
    private void Start()
    {
        GameEvents.current.ArriveeInstantiated(gameObject);
    }

    private void OnDestroy()
    {
        GameEvents.current.ArriveeDestroyed();
    }
}
