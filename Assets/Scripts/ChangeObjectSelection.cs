﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.XR.Interaction.Toolkit.AR;

public class ChangeObjectSelection : MonoBehaviour
{

    [SerializeField] private Button buttonDepart;
    [SerializeField] private Button buttonCheckpoint;
    [SerializeField] private Button buttonArrivee;
    [SerializeField] private Button buttonDemarrer;

    [SerializeField] private ARPlacementInteractable ARPlacement;

    public GameObject depart;
    public GameObject checkpoint;
    public GameObject arrivee;

    public Text textDebug;

    private bool arriveePlaced = false;
    private bool departPlaced = false;

    private void Awake()
    {
        buttonDepart.onClick.AddListener(onClickDepart);
        buttonCheckpoint.onClick.AddListener(onClickCheckpoint);
        buttonArrivee.onClick.AddListener(onClickArrivee);
        buttonDemarrer.onClick.AddListener(onClickDemarrer);
    }

    private void Start()
    {
        GameEvents.current.onDepartInsantiated += DepartInstantiated;
        GameEvents.current.onDepartDestroyed += DepartDestroyed;
        GameEvents.current.onArriveeInstantiated += ArriveeInstantiated;
        GameEvents.current.onArriveeDestroyed += ArriveeDestroyed;
    }

    private void Update()
    {
        textDebug.text = "Objet actuel : " + ARPlacement.placementPrefab.name;
    }

    private void onClickDepart()
    {
        ARPlacement.placementPrefab = depart;
    }

    private void onClickCheckpoint()
    {
        ARPlacement.placementPrefab = checkpoint;
    }

    private void onClickArrivee()
    {
        ARPlacement.placementPrefab = arrivee;

    }

    private void onClickDemarrer()
    {
        

        if (arriveePlaced && departPlaced)
        {
            textDebug.text += "DEBUT";
            GameEvents.current.GameStart();
        }
        else
        {
            //TODO : Coroutine qui affiche du texte pour dire qu'il faut depart et arrivee
        }
        
    }

    private void DepartInstantiated(GameObject obj)
    {
        departPlaced = true;
        ARPlacement.placementPrefab = null;
        buttonDepart.interactable = false;
        textDebug.text += "DEPART OK";
    }

    private void DepartDestroyed()
    {
        departPlaced = false;
        buttonDepart.interactable = true;
    }

    private void ArriveeInstantiated(GameObject obj)
    {
        arriveePlaced = true;
        ARPlacement.placementPrefab = null;
        buttonArrivee.interactable = false;
    }

    private void ArriveeDestroyed()
    {
        arriveePlaced = false;
        buttonArrivee.interactable = true;
    }
}
