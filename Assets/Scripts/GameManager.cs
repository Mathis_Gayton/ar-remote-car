﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.AR;

public class GameManager : MonoBehaviour
{

    public static GameManager current;

    [SerializeField] private GameObject canvasMenu;
    [SerializeField] private GameObject canvasJoystick;
    [SerializeField] private GameObject car;
    [SerializeField] ARPlacementInteractable ARPlacement;

    private List<GameObject> checkpoints;
    private GameObject depart;
    private GameObject arrivee;

    private void Awake()
    {
        current = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        checkpoints = new List<GameObject>();

        GameEvents.current.onGameStart += Setup;
        GameEvents.current.onDepartInsantiated += SetDepart;
        GameEvents.current.onDepartDestroyed += DeleteDepart;
        GameEvents.current.onArriveeInstantiated += SetArrivee;
        GameEvents.current.onArriveeDestroyed += DeleteArrivee;

        ARPlacement.objectPlaced.AddListener(ARPlaced);
    }

    private void ARPlaced(ARObjectPlacementEventArgs args)
    {
        if(args.placementObject.CompareTag("Checkpoint"))
        {
            checkpoints.Add(args.placementObject);
        }
        
    }

    private void SetDepart(GameObject obj)
    {
        depart = obj;
    }

    private void DeleteDepart()
    {
        depart = null;
    }

    private void SetArrivee(GameObject obj)
    {
        arrivee = obj;
    }
    
    private void DeleteArrivee()
    {
        arrivee = null;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Setup()
    {

        
        //Instantiate(car, Vector3.zero, Quaternion.identity);

        if (depart != null && arrivee != null)
        {
            canvasMenu.SetActive(false);
            canvasJoystick.SetActive(true);
            Instantiate(car, depart.transform.position + new Vector3(0,2,0), depart.transform.rotation);
        }
    }
}
