﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarMove : MonoBehaviour
{

    [SerializeField] private FixedJoystick joystick;
    [SerializeField] private Text textDebugJoystick;
    private Rigidbody rb;
    public float speed = 5f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        joystick = GameObject.FindGameObjectWithTag("Joystick").GetComponent<FixedJoystick>();
        textDebugJoystick = GameObject.Find("Text Debug Joystick").GetComponent<Text>();
    }

    private void FixedUpdate()
    {
        Vector3 direction = Vector3.forward * joystick.Vertical + Vector3.right * joystick.Horizontal;
        textDebugJoystick.text = direction.ToString() + " " + (direction * speed).ToString() + " " + rb.mass;
        rb.AddForce(direction * speed, ForceMode.Acceleration);
    }
}
