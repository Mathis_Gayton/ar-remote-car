﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents current;

    private void Awake()
    {
        Debug.Log("AWAKE EVENTS");
        current = this;
    }

    public event Action<GameObject> onDepartInsantiated;
    public void DepartInstantiated(GameObject obj)
    {
        if(onDepartInsantiated != null)
        {
            onDepartInsantiated(obj);
        }
    }

    public event Action onDepartDestroyed;
    public void DepartDestroyed()
    {
        if(onDepartDestroyed != null)
        {
            onDepartDestroyed();
        }
    }

    public event Action<GameObject> onArriveeInstantiated;
    public void ArriveeInstantiated(GameObject obj)
    {
        if (onArriveeInstantiated != null)
        {
            onArriveeInstantiated(obj);
        }
    }

    public event Action onArriveeDestroyed;
    public void ArriveeDestroyed()
    {
        if (onArriveeDestroyed != null)
        {
            onArriveeDestroyed();
        }
    }

    public event Action onGameStart;
    public void GameStart()
    {
        if(onGameStart != null)
        {
            onGameStart();
        }
    }
}
