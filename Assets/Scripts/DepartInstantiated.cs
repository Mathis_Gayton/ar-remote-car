﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepartInstantiated : MonoBehaviour
{
    private void Start()
    {
        GameEvents.current.DepartInstantiated(gameObject);
    }

    private void OnDestroy()
    {
        GameEvents.current.DepartDestroyed();
    }
}
