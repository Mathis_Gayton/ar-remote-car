﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.UI;

public class TapToPlace : MonoBehaviour
{

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();
    ARRaycastManager m_RaycastManager;
    ARReferencePointManager m_ReferencePointManager;
    List<ARReferencePoint> m_ReferencePoint;
    ARPlaneManager m_PlaneManager;

    [SerializeField] private GameObject start;
    [SerializeField] private GameObject checkpoint;
    [SerializeField] private GameObject finish;

    public Text textDebug;
    private bool isDragging = false;


    private void Awake()
    {
        m_RaycastManager = GetComponent<ARRaycastManager>();
        m_ReferencePointManager = GetComponent<ARReferencePointManager>();
        m_PlaneManager = GetComponent<ARPlaneManager>();
        m_ReferencePoint = new List<ARReferencePoint>();
        m_ReferencePointManager.referencePointPrefab = checkpoint;
    }

    // Update is called once per frame
    void Update()
    {
        if(!TryGetTouchPosition(out Vector2 touchPosition))
        {
            return;
        }


        if (m_RaycastManager.Raycast(touchPosition, s_Hits, TrackableType.PlaneWithinPolygon))
        {
            var hitPose = s_Hits[0].pose;
            TrackableId planeId = s_Hits[0].trackableId;

            if(!isDragging)
            {
                var ReferencePoint = m_ReferencePointManager.AttachReferencePoint(m_PlaneManager.GetPlane(planeId), hitPose);
                if (ReferencePoint != null)
                {
                    //RemoveAllReferencePoints();
                    m_ReferencePoint.Add(ReferencePoint);
                }
            }
        }
        
    }

    public void RemoveAllReferencePoints()
    {
        foreach(var referencePoint in m_ReferencePoint)
        {
            m_ReferencePointManager.RemoveReferencePoint(referencePoint);
        }

        m_ReferencePoint.Clear();
    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if(Input.touchCount > 0)
        {

            touchPosition = Input.GetTouch(0).position;

            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                isDragging = false;
            }
            else if(Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                isDragging = true;
            }

            return true;
            
        }

        touchPosition = default;
        return false;
    }
}
